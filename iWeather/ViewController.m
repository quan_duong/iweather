//
//  ViewController.m
//  iWeather
//
//  Created by Duong Tien Quan on 11/4/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *location;

@property (weak, nonatomic) IBOutlet UIButton *temperature;
@property (weak, nonatomic) IBOutlet UIImageView *weather;
@property (weak, nonatomic) IBOutlet UILabel *quote;
@property (weak, nonatomic) IBOutlet UILabel *unitTemperature;

@end

@implementation ViewController
{
    NSArray* quotes;
    NSArray* locations;
    NSArray* photoFiles;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    quotes = @[@"Một con ngựa đau cả tàu bỏ cỏ",@"Có công mài sắt có ngày nên kim",@"Chớ thấy sóng mà ngã tay chèo",@"Đi một ngày đàng học một sàng khôn"];
    locations = @[@"Hanoi",@"Ho chi minh",@"Hue",@"Da Nang"];
    photoFiles = @[@"rain",@"sunny",@"thunder",@"windy"];
    
}
- (IBAction)updateWeather:(id)sender {
    int quoteIndex= arc4random_uniform(quotes.count);
    NSLog(@"%d",quoteIndex);
    self.quote.text = quotes[quoteIndex];
    int quoteLocation = arc4random_uniform(locations.count);
    self.location.text = locations[quoteLocation];
    int weatherIndex = arc4random_uniform(photoFiles.count);
    self.weather.image = [UIImage imageNamed:photoFiles[weatherIndex]];
    
    
    NSString* string = [NSString stringWithFormat:@"%2.1f",[self getNewCTemperature]];
    [self.temperature setTitle:string forState:UIControlStateNormal];
    
}

- (float) getNewCTemperature{
    self.unitTemperature.text = @"C";
    return 14.0 +arc4random_uniform(18) +(float)arc4random()/(float) INT32_MAX;
}

-(float) getFTemperature:(float) tempC{
    //°F  =  ( °C × 1.8 ) +  32
    self.unitTemperature.text = @"F";
    float tempF = tempC*1.8 +32;
    return tempF;
}

-(float) getCTemperature:(float) tempF{
    //°C  =  ( °F ─  32 )  ⁄  1.8
    self.unitTemperature.text = @"C";
    float tempC = (tempF - 32)/1.8;
    return tempC;
}

- (IBAction)updateF:(id)sender {
    
   
    UIButton *someButton = (UIButton*)sender;
    
    NSLog(@"The button title is %@ ", [someButton titleForState:UIControlStateNormal]);
    float temp = [[someButton titleForState:UIControlStateNormal] floatValue];
        NSString* string;
    if ([self.unitTemperature.text isEqualToString:@"C"]) {
        string = [NSString stringWithFormat:@"%2.1f",[self getFTemperature:temp]];
    }else{
        string = [NSString stringWithFormat:@"%2.1f",[self getCTemperature:temp]];
    }
    [self.temperature setTitle:string forState:UIControlStateNormal];
    
}


@end
